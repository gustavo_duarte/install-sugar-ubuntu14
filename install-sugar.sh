#!/bin/sh


echo "" >> /etc/apt/sources.list
echo "# Repositorio OLPC para Sugar." >> /etc/apt/sources.list
echo "deb [trusted=yes] http://dev.laptop.org/pub/us trusty main" >> /etc/apt/sources.list

apt-get -y update
apt-get -y install olpc-ubuntu-sugar

